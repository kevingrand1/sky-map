/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{vue,js,scss}',
  ],

  theme: {
    container: {
      center: true,
    },
    screens: {
      md: "768px",
      lg: "992px",
      xl: "1200px",
    },
    colors: {
      white: '#fff',
      black: '#000',

      grey: {
        98: '#F9F9F9',
        94: '#F0F0F0',
        88: '#E0E0E0',
        80: '#CCCCCC',
        63: '#CCCCCC',
        40: '#666666',
        11: '#1C1C1C',
      },

      yellow: {
        light: '#F9BF29',
        DEFAULT: '#F6B50D',
      },

      beige: '#FBF5E6',
      green: '#3F8972',
      blue: '#5033D1',
      purple: '#C3B0F9',
      red: '#ED5F52',

      success: '#009C51',
      alert: '#E71D1D',
      info: '#4264BD',
    },
    fontFamily: {
      'clash-display': ['Clash Display', 'sans-serif'],
      'pathway-extreme': ['Pathway Extreme', 'sans-serif'],
      'general-sans': ['General Sans', 'sans-serif'],
    },
    extend: {},
  },
}
